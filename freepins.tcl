#!/opt/ActiveTcl-8.6/bin/tclsh
# Hey Emacs, use -*- Tcl -*- mode

set scriptname [file rootname $argv0]

# ---------------------- Command line parsing -------------------------
package require cmdline
set usage "usage: [file tail $argv0] \[options] source-root"
set options {
    {t.arg "dip28" "ATmega package"}
    {t?            "List known package types"}
}

try {
    array set params [::cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {msg o} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $msg
    exit 1
}

proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterint 0 $width] {
	append dashline "-"
    }
    return $dashline
}

if $params(t?) {
    # List the known package types
    set package_width 20
    set description_width 20
    set format_string "%-*s %-*s"
    set header [format $format_string $package_width "Package" \
		    $description_width "Description"]
    puts $header
    puts [dashline [string length $header]]
    puts [format $format_string $package_width "dip28" \
	      $description_width \
	      "28-pin Dual Inline Package"]    

    exit
}

# After cmdline is done, argv will point to the last argument
if {[llength $argv] == 1} {
    set input_path $argv
} else {
    puts [cmdline::usage $options $usage]
    exit 1
}


proc read_file {filename} {
    # Return a list of lines from an input file
    #
    # The file consists of capacitance measurements.
    if { [catch {open $filename r} fid] } {
	    puts "Could not open file $filename"
	return
    }
    set datafile_list [split [read $fid] "\n"]
    return $datafile_list
}

proc get_port_strings {portname} {
    # Return a list of strings to match using the port name, like
    # portb5
    set portnumber [regexp -inline {[0-9]} $portname]
    set portletter [string index $portname [expr [string last $portnumber $portname] -1]]
    set string_list [list]
    # The original port name
    lappend string_list "port$portletter$portnumber"
    # Data direction settings
    lappend string_list "dd$portletter$portnumber"
    if {[string match $portletter$portnumber d1]} {
	# This port is also used for the USART Tx pin
	lappend string_list "txen0"
    }
    if {[string match $portletter$portnumber d0]} {
	# This port is also used for the USART Rx pin
	lappend string_list "rxen0"
    }
    if {[string match $portletter$portnumber c1]} {
	# This port is also used for the ADC (mux position 0)
	lappend string_list "mux0"
    }
    if {[string match $portletter$portnumber c4]} {
	# This port is also used for the I2C-SDA pin
	lappend string_list "twen"
    }
    if {[string match $portletter$portnumber c5]} {
	# This port is also used for the I2C-SCL pin
	lappend string_list "twen"
    }
    return $string_list
}

proc get_timer_strings {timername} {
    # Return a list of strings to mach using the timer name, like
    # timer0
    set timernumber [regexp -inline {[0-9]} $timername]
    set string_list [list]
    # The original timer name
    lappend string_list "timer$timernumber"
    # Timer counter control register references
    lappend string_list "tccr${timernumber}a"
    lappend string_list "tccr${timernumber}b"
    return $string_list
}

proc pindict_init {} {
    # Return an initialized port pin dictionary
    set pindict [dict create]
    dict set pindict "inuse" false
    return $pindict
}

proc timerdict_init {} {
    # Return an initialized timer dictionary
    set timerdict [dict create]
    dict set timerdict "inuse" false
    return $timerdict
}

proc iterlist {size} {
    # Return a list of integers starting at 0 and counting up to (size
    # -1)
    set iterlist [list]
    set count 0
    while {$count < $size} {
	lappend iterlist $count
	incr count
    }
    return $iterlist
}

proc center_text {text width} {
    # Return text centered in a string of size width
    set centered ""
    set text_width [string length $text]
    set pad [expr int(($width - $text_width)/double(2))]
    foreach padchar [iterlist $pad] {
	append centered " "
    }
    append centered $text
    foreach padchar [iterlist $pad] {
	append centered " "
    }
    return $centered
}

proc left_justify {text width} {
    # Return text left-justified in a string of size width
    set justified ""
    set text_width [string length $text]
    set pad [expr int(($width - $text_width))]
    append justified $text
    foreach padchar [iterlist $pad] {
	append justified " "
    }
    return $justified
}

proc right_justify {text width} {
    # Return text right-justified in a string of size width
    set justified ""
    set text_width [string length $text]
    set pad [expr int(($width - $text_width))]
    foreach padchar [iterlist $pad] {
	append justified " "
    }
    append justified $text
    return $justified
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterlist $width] {
	append dashline "-"
    }
    return $dashline
}

########################### Package types ############################

if [string match "dip28" $params(t)] {
    # DIP-28 package
    #
    # Number of B port pins
    set bports 6

    # Number of C port pins
    set cports 6

    # Number of D port pins
    set dports 8
}

if [string match "tqfp32" $params(t)] {
    # TQFP-32 package
    #
    # Number of B port pins
    set bports 6

    # Number of C port pins
    set cports 7

    # Number of D port pins
    set dports 8
}


############################### Port B ###############################

foreach pin_number [iterint 0 $bports] {
    dict set portpin_dict portb$pin_number [pindict_init]
}

############################### Port C ###############################

foreach pin_number [iterint 0 $cports] {
    dict set portpin_dict portc$pin_number [pindict_init]
}

############################### Port D ###############################

foreach pin_number [iterint 0 $dports] {
    dict set portpin_dict portd$pin_number [pindict_init]
}

############################### Timers ###############################

# Timer 0
dict set timer_dict timer0 [timerdict_init]

# Timer 1
dict set timer_dict timer1 [timerdict_init]

# Timer 2
dict set timer_dict timer2 [timerdict_init]



set source_files [list]
foreach object_file [split [glob -nocomplain -directory $input_path *.o]] {
    lappend source_files [file rootname $object_file].c
}

foreach source_file $source_files {
    puts "Analyzing $source_file"
    puts ""
    try {
	set fp [open $source_file r]
    } trap {} {message opdict} {
	# message will be the human-readable error.  optdict will be the
	# options dictionary present when the command was attempted.
	puts "$message"
	puts [dict get $optdict -errorcode]
	continue
    }
    set line_list [split [read $fp] "\n"]
    close $fp
    set incomment false
    foreach line $line_list {
	if {[string length [string trim $line]] == 0} {
	    # This is an empty line
	    continue
	}
	# Look for multi-line comment start
	set multiline_start_index [string first "/*" $line]
	# Look for multi-line comment stop
	set multiline_stop_index [string first "*/" $line]
	# Look for single-line comment start
	set singleline_start_index [string first "//" $line]
	if {$multiline_stop_index == -1 && $incomment} {
	    # We're inside a comment that doesn't end on this line
	    continue
	}
	if {[string first "\#" [string trim $line]] == 0} {
	    # This is an include statement
	    continue
	}
	if {$multiline_stop_index >= 0 && $incomment} {
	    # We're inside a commment that ends on this line.  Grab
	    # everything after the comment pattern.
	    set cleanline [string trim [string range $line $multiline_stop_index end]]
	    set incomment false
	} elseif {$multiline_start_index >= 0} {
	    # A multiline comment starts on this line.  Grab
	    # everything up to the comment pattern.
	    set cleanline [string trim [string range $line 0 $multiline_start_index]]
	    if {$multiline_stop_index >= 0} {
		# The multinline comment started on this line also ends on this line
		set incomment false
	    } else {
		set incomment true		
	    }
	} elseif {$singleline_start_index >= 0} {
	    # There's a single line comment in this line.  Grab
	    # everything up to the comment pattern.
	    set cleanline [string trim [string range $line 0 $singleline_start_index]]
	    if {[string length [string trim $cleanline]] == 1} {
		# We're left with just a /
		continue
	    }
	} else {
	    # No comments at all apply
	    set cleanline $line
	}
	puts $cleanline
	# Analyze pins
	foreach pin [dict keys $portpin_dict] {
	    if {[string match "used" [dict get $portpin_dict $pin]]} {
		# This pin is in use -- no point in searching for it
		continue
	    }
	    foreach match_string [get_port_strings $pin] {
		if {[regexp -nocase $match_string $cleanline]} {
		    # We've found a reference to this pin
		    puts "$pin used in $source_file"
		    puts $cleanline
		    if [dict exists [dict get $portpin_dict $pin] "inuse"] {
			# The pin has its own dictionary. We can
			# modify the dictionary with an intermediate
			# dummy variable.
			set dummy_dict [dict get $portpin_dict $pin]
			dict set dummy_dict "inuse" "true"
			dict set dummy_dict "match" $match_string
			dict set portpin_dict $pin $dummy_dict

		    } else {
			dict set portpin_dict $pin "used"
		    }

		}
	    }
	}
	# Analyze timers
	foreach timer [dict keys $timer_dict] {
	    if {[string match "used" [dict get $timer_dict $timer]]} {
		# This timer is in use -- no point in searching for it
		continue
	    }
	    foreach match_string [get_timer_strings $timer] {
		if {[regexp -nocase $match_string $cleanline]} {
		    # We've found a reference to this timer
		    puts "$timer used in $source_file"
		    puts $cleanline
		    if [dict exists [dict get $timer_dict $timer] "inuse"] {
			# The timer has its own dictionary already.
			# We can modify the dictionary with an
			# intermediate variable.
			set dummy_dict [dict get $timer_dict $timer]
			dict set dummy_dict "inuse" "true"
			dict set dummy_dict "match" $match_string
			dict set timer_dict $timer $dummy_dict

		    } else {
			dict set timer_dict $timer "used"
		    }

		}
	    }
	}
    }
    puts ""
}

set resource_column_width 10
set status_column_width 10
set match_width 10

# - -- left justify
# * -- minimum width is specified as argument
# s -- format as a string (no conversion)
set format_string "%-*s %-*s     %-*s"
set header [format $format_string \
		$resource_column_width "Pin" \
		$status_column_width [center_text "Used/Free" $status_column_width]  \
		$match_width "Match"]
puts $header
puts [dashline [string length $header]]

# Report found pins
foreach pin [dict keys $portpin_dict] {
    set inuse false
    set pindict [dict get $portpin_dict $pin]
    if [dict exists [dict get $portpin_dict $pin] "inuse"] {
	set inuse [dict get [dict get $portpin_dict $pin] "inuse"]
    }
    if $inuse {
	puts [format $format_string \
		  $resource_column_width $pin \
		  $status_column_width [right_justify "used" $status_column_width] \
		  $match_width [dict get $pindict "match"]]
    } else {
	puts [format "%-*s %-*s" \
		  $resource_column_width $pin \
		  $status_column_width [left_justify "free" $status_column_width]]	  
    }
}

# Report timers
puts ""
set header [format $format_string \
		$resource_column_width "Timer" \
		$status_column_width [center_text "Used/Free" $status_column_width]  \
		$match_width "Match"]
puts $header
puts [dashline [string length $header]]

foreach timer [dict keys $timer_dict] {
    set inuse false
    set timerdict [dict get $timer_dict $timer]
    if [dict exists [dict get $timer_dict $timer] "inuse"] {
	set inuse [dict get [dict get $timer_dict $timer] "inuse"]
    }
    if $inuse {
	puts [format $format_string \
		  $resource_column_width $timer \
		  $status_column_width [right_justify "used" $status_column_width] \
		  $match_width [dict get $timerdict "match"]]
    } else {
	puts [format "%-*s %-*s" \
		  $resource_column_width $timer \
		  $status_column_width [left_justify "free" $status_column_width]]
    }
}
